const mqtt = require("mqtt");
const { host, port, username, password } = require("../config/mqtt.json");

const mqttClient = mqtt.connect(`mqtt://${host}:${port}`, {
  username,
  password,
});

module.exports = mqttClient;
