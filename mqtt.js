"use strict";

const mqttClient = require("./services/mqtt.service");
const header = "\xA6"; // '¦'
const footer = "\xA9"; // '©'
const Gate = require("./models/gate");
const commands = require("./commands").map((c) => c.command);
commands.push("cameraIn", "cameraOut");
const isCommand = new RegExp(`^(${commands.join("|")}).*`);
let gates = [];

mqttClient.on("connect", () => {
  console.log("Connected to MQTT broker");

  mqttClient.subscribe("command", (err) => {
    if (err) console.error(err);
    else console.log(`Subscribed to command topic`);
  });

  Gate.findAll((err, allGates) => {
    if (err) return console.error(err);
    gates = allGates;
    allGates.forEach((gate) => {
      gate.connect();
    });
  });
});

mqttClient.on("message", (topic, payload) => {
  if (topic == "command") {
    const { host, command } = JSON.parse(payload.toString());

    if (!isCommand.test(command)) {
      return console.error(`Invalid command: ${command}`);
    }

    console.log(`Received command ${command} to ${host}`);

    const gate = gates.find((g) => g.host == host);
    if (!gate) return console.error("Invalid gate");

    let commandBuffer = Buffer.from(`${header}${command}${footer}`);

    if (command.startsWith("DS")) {
      const [line1, line2] = command.slice(2).split("|");
      // brightness = 9
      // baris = 1 (tampil normal)
      // blinking duration baris 1= 30
      // running text speed baris 2 = 03
      commandBuffer = Buffer.from(
        `${header}DSD913003${line1}|13003${line2}${footer}`
      );
    }

    if (command.startsWith("camera")) {
      gate.getSnapshot(command, (err, base64String) => {
        if (err) return console.error(err);
        const publishedData = JSON.stringify({
          type: "camera",
          host: gate.host,
          name: gate.name,
          // TODO
          // gate: 'IN',
          snapshotBase64String: base64String,
        });

        console.log(`Publish ${publishedData}`);
        mqttClient.publish("access", publishedData);
      });
    }

    gate.client.write(commandBuffer);
  }
});

module.exports = { mqttClient };
