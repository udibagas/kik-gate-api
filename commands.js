module.exports = [
  { command: "OUT1ON", description: "relay 1 on", response: "OUT1ONOK" },
  { command: "OUT2ON", description: "relay 2 on", response: "OUT1ONOK" },
  { command: "OUT3ON", description: "relay 3 on", response: "OUT3ONOK" },
  { command: "OUT4ON", description: "relay 4 on", response: "OUT4ONOK" },
  { command: "OUT1OFF", description: "relay 1 off", response: "OUT1OFFOK" },
  { command: "OUT2OFF", description: "relay 2 off", response: "OUT2OFFOK" },
  { command: "OUT3OFF", description: "relay 3 off", response: "OUT3OFFOK" },
  { command: "OUT4OFF", description: "relay 4 off", response: "OUT4OFFOK" },
  {
    command: "OPEN1",
    description: "relay 2 off, relay 3 off, relay 1 on 1 detik, lalu off",
    response: "OPEN1OK",
  },
  {
    command: "CLOSE1",
    description: "relay 1 off, relay 3 off, relay 2 on 1 detik, lalu off",
    response: "CLOSE1OK",
  },
  {
    command: "STOP1",
    description: "relay 1 off, relay 2 off, relay 3 on 1 detik, lalu off",
    response: "STOP1OK",
  },
  {
    command: "OPEN2",
    description: "relay 1 on, relay 2 off",
    response: "OPEN2OK",
  },
  {
    command: "CLOSE2",
    description: "relay 2 on, relay 1 off",
    response: "CLOSE2OK",
  },
  {
    command: "STOP2",
    description: "relay 1 off, relay 2 off",
    response: "STOP2OK",
  },
  {
    command: "PING",
    description:
      "mengecek apakah device berfungsi atau tidak. Jika tidak ada respon, maka ada problem pada device atau koneksi",
    response: "PINGOK",
  },
  {
    command: "STAT",
    description: "mengecek status input dan output",
    response: "STATabcdefgh",
  },
  {
    command: "TRIG1",
    description: "relay 1 on 1 detik, lalu off",
    response: "TRIG1OK",
  },
  {
    command: "TRIG2",
    description: "relay 2 on 1 detik, lalu off",
    response: "TRIG2OK",
  },
  {
    command: "TRIG3",
    description: "relay 3 on 1 detik, lalu off",
    response: "TRIG3OK",
  },
  {
    command: "TRIG4",
    description: "relay 4 on 1 detik, lalu off",
    response: "TRIG4OK",
  },
  {
    command: "MT",
    description: "play mp3",
    response: "PLAYEND/NOTRACK",
    example: "MT00001",
  },
  {
    command: "DS",
    description: "runing text",
    response: "DSOK",
    example: "DSTEST|Running Text",
  },
];
