const ConfigController = require("../controllers/config.controller");
const GateController = require("../controllers/gate.controller");
const router = require("express").Router();
const validate = require("../middlewares/validation");
const gateSchema = require("../validations/gate.validation");
const mqttConfigSchema = require("../validations/mqtt.validation");
const authConfigSchema = require("../validations/auth.validation");
const AuthController = require("../controllers/auth.controller");

router.get("/gates", GateController.list);
router.get("/gates/:id", GateController.show);
router.post("/gates", validate(gateSchema), GateController.create);
router.put("/gates/:id", validate(gateSchema), GateController.update);
router.delete("/gates/:id", GateController.delete);
router.post("/mqtt/restart", ConfigController.restartMqttService);

router.get("/config/mqtt", ConfigController.getMqttConfig);
router.put(
  "/config/mqtt",
  validate(mqttConfigSchema),
  ConfigController.setMqttConfig
);

router.get("/auth", AuthController.index);
router.put("/auth", validate(authConfigSchema), AuthController.update);

module.exports = router;
