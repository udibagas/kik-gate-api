const express = require("express");
const router = express.Router();
const multer = require("multer");
const AuthController = require("../controllers/auth.controller");
const ConfigController = require("../controllers/config.controller");
const GateController = require("../controllers/gate.controller");
const Controller = require("../controllers/index.controller");
const LprController = require("../controllers/lpr.controller");
const auth = require("../middlewares/auth");

const storage = multer.memoryStorage();
const upload = multer({
  storage,
  limits: {
    fieldSize: 10 * 1024 * 1024,
  },
});
// const upload = multer({ dest: "uploads/" });
router.post("/", upload.single("anpr_xml"), LprController.index);
router.get("/anpr", LprController.anpr);

router.get("/login", AuthController.showLoginForm);
router.post("/login", AuthController.login);

router.use(auth);

router.get("/", Controller.home);
router.get("/logout", AuthController.logout);
router.get("/gates", GateController.index);
router.get("/gates/camera/:id/:camera", GateController.camera);
router.get("/config/mqtt", ConfigController.mqtt);
router.get("/config/auth", ConfigController.auth);
router.use("/api", require("./api"));

module.exports = router;
