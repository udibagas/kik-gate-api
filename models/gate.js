"use strict";
const fs = require("fs");
const { Socket } = require("net");
const DigestFetch = require("digest-fetch");
const responses = require("../responses").map((r) => r.response);
const isResponse = new RegExp(`^(${responses.join("|")}).*`);
const mqttClient = require("../services/mqtt.service");
const { exec } = require("child_process");

class Gate {
  static path = "./data/gates.json";

  #client;

  constructor(id, host, port, name, cameraIn, cameraOut) {
    this.id = +id;
    this.host = host;
    this.port = +port;
    this.name = name;
    this.cameraIn = cameraIn;
    this.cameraOut = cameraOut;
    this.#client = null;
  }

  get client() {
    return this.#client;
  }

  reconnect() {
    this.client.removeAllListeners();
    this.client.destroy();

    setTimeout(() => {
      this.connect();
    }, 1000);
  }

  connect() {
    const client = new Socket();
    const { port, host, name } = this;
    client.connect(port, host, () => {
      console.log(`Connected to ${name}`);
    });

    client.on("data", (bufferData) => {
      // remove header and footer
      data = bufferData.toString().slice(1, -1);

      if (!isResponse.test(data)) {
        return console.log(`Get command from ${name}: ${data}`);
      }

      // kalau bagian dari response, publish biar bisa di subscribe
      console.log(`Get response from ${name}: ${data}`);
      const response = { message: data };

      if (data[0] == "W" || data[0] == "X") {
        // read card
        response.type = "card";
        const cardNumber = parseInt(data.slice(1), 16).toString();
        response.cardNumber = cardNumber;
        response.message = "Card detected";

        // gate in
        if (data[0] == "W") {
          response.gate = "IN";
        }

        // gate out
        if (data[0] == "X") {
          response.gate = "OUT";
        }
      }

      // qr code
      if (data.slice(0, 2) == "PT" || data.slice(0, 2) == "QT") {
        response.type = "qr";
        response.port = data.slice(0, 2) == "PT" ? 1 : 2;
        response.message = data.slice(2);
      }

      if (data == "TRIG1OK") {
        response.type = "gate";
        response.gate = "IN";
        response.message = "Gate in opened";
      }

      if (data == "TRIG2OK") {
        response.type = "gate";
        response.gate = "OUT";
        response.message = "Gate out opened";
      }

      const publishedData = JSON.stringify({ host, name, ...response });
      console.log(`Publish ${publishedData}`);
      mqttClient.publish("access", publishedData);
    });

    client.on("error", () => {
      console.error(`Connection error. Reconnecting to ${name}...`);
      this.reconnect();
    });

    client.on("close", () => {
      console.error(`Connection closed. Reconnecting to ${name}...`);
      this.reconnect();
    });

    this.#client = client;
  }

  getSnapshot(camera, cb) {
    const { snapshotUrl, username, password, authType } = this[camera];

    const client = new DigestFetch(username, password, {
      basic: authType == "Basic",
    });

    client
      .fetch(snapshotUrl)
      .then((response) => {
        if (response.ok) return response.buffer();
        throw response;
      })
      .then((buffer) => {
        cb(null, buffer.toString("base64"));
      })
      .catch((err) => {
        console.error(err);
        cb({ message: "Error get snapshot", err });
      });
  }

  static findAll(cb) {
    fs.readFile(Gate.path, "utf-8", (err, data) => {
      if (err) return cb(err);
      const gates = JSON.parse(data).map((el) => {
        const { id, host, port, name, cameraIn, cameraOut } = el;
        return new Gate(id, host, port, name, cameraIn, cameraOut);
      });
      cb(null, gates);
    });
  }

  static find(id, cb) {
    this.findAll((err, gates) => {
      if (err) return cb(err);
      const gate = gates.find((el) => el.id == id);
      if (!gate) return cb(`Invalid gate`);
      cb(null, gate);
    });
  }

  static findByHost(host, cb) {
    this.findAll((err, gates) => {
      if (err) return cb(err);
      const gate = gates.find((el) => el.host == host);
      if (!gate) return cb(`Invalid gate`);
      cb(null, gate);
    });
  }

  static create(data, cb) {
    this.findAll((err, gates) => {
      if (err) return cb(err);
      const { host, port, name, cameraIn, cameraOut } = data;
      const id = gates.length ? gates.at(-1).id + 1 : 1;
      const gate = new Gate(id, host, port, name, cameraIn, cameraOut);
      gates.push(gate);

      this.save(gates, (err) => {
        if (err) return cb(err);
        cb(null, gate);
      });
    });
  }

  static delete(id, cb) {
    this.findAll((err, gates) => {
      if (err) return cb(err);
      const gateIndex = gates.findIndex((el) => el.id == id);
      if (gateIndex == -1) return cb(`Invalid gate`);
      const gate = gates[gateIndex];
      gates.splice(gateIndex, 1);

      this.save(gates, (err) => {
        if (err) return cb(err);
        cb(null, gate);
      });
    });
  }

  static update(id, data, cb) {
    this.findAll((err, gates) => {
      if (err) return cb(err);
      const gateIndex = gates.findIndex((el) => el.id == id);
      if (gateIndex == -1) return cb(`Invalid gate`);
      gates[gateIndex] = { id, ...data };
      this.save(gates, (err) => {
        if (err) return cb(err);
        cb(null, gateIndex[gateIndex]);
      });
    });
  }

  static save(gates, cb) {
    const string = JSON.stringify(gates, null, 2);
    fs.writeFile(Gate.path, string, (err) => {
      if (err) return cb(err);
      exec("pm2 restart mqtt", (err) => {
        if (err) console.error(err);
      });

      cb(null);
    });
  }
}

module.exports = Gate;
