const Validator = require("fastest-validator");

const v = new Validator({
  messages: {
    required: "{field} is required",
    ipv4: "{field} must be valid IP Address",
  },
});

const validate = (schema) => {
  return (req, res, next) => {
    const check = v.compile(schema);
    const validation = check(req.body);
    if (validation === true) return next();

    const errors = {};

    validation.forEach((e) => {
      typeof errors[e.field] === "undefined"
        ? (errors[e.field] = [e.message])
        : errors[e.field].push(e.message);
    });

    return res
      .status(400)
      .json({ status: 400, name: "ValidationError", errors });
  };
};

module.exports = validate;
