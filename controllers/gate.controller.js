"use strict";

const Gate = require("../models/gate");
const ejs = require("ejs");

class GateController {
  static index(req, res) {
    Gate.findAll((err, gates) => {
      if (err) return res.send(err);
      ejs.renderFile("./views/gates/index.ejs", (err, content) => {
        if (err) return res.send(err);
        res.render("_layout", { content });
      });
    });
  }

  static list(req, res) {
    Gate.findAll((err, gates) => {
      if (err) return res.send(err);
      res.json(gates);
    });
  }

  static show(req, res) {
    Gate.find(req.params.id, (err, gate) => {
      if (err) return res.send(err);
      res.json(gate);
    });
  }

  static create(req, res) {
    Gate.create(req.body, (err, gate) => {
      if (err) return res.send(err);
      res.json({
        message: "Data has been saved",
        data: gate,
      });
    });
  }

  static update(req, res) {
    Gate.update(req.params.id, req.body, (err, gate) => {
      if (err) return res.send(err);
      res.json({
        message: "Gate has been updated",
        data: gate,
      });
    });
  }

  static delete(req, res) {
    Gate.delete(req.params.id, (err) => {
      if (err) return res.send(err);
      res.json({ message: "Gate has been deleted" });
    });
  }

  static camera(req, res) {
    const { id, camera } = req.params;
    Gate.find(id, (err, gate) => {
      if (err) return res.send(err);
      gate.getSnapshot(camera, (err, base64String) => {
        if (err) return res.send(err);
        res.render("gates/camera", { src: base64String });
      });
    });
  }
}

module.exports = GateController;
