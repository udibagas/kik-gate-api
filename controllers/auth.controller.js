"use strict";

const bcrypt = require("bcryptjs");
const fs = require("fs");

class AuthController {
  static index(req, res) {
    const { username } = require("../config/auth.json");
    res.json({ username });
  }

  static showLoginForm(req, res) {
    if (req.session.user) return res.redirect("/");

    const { username, password } = req.body;
    const { error } = req.query;
    res.render("login", { username, password, error });
  }

  static login(req, res) {
    fs.readFile("./config/auth.json", "utf-8", (err, data) => {
      if (err) return res.status(500).send(err);
      const { username, password } = req.body;
      const auth = JSON.parse(data);

      if (!username || !password) {
        const error = "Username and password is required";
        return res.render("login", { username, password, error });
      }

      if (
        username !== auth.username ||
        !bcrypt.compareSync(password, auth.password)
      ) {
        const error = "Invalid username or password";
        return res.render("login", { username, password, error });
      }

      req.session.user = auth.username;
      res.redirect("/");
    });
  }

  static logout(req, res) {
    req.session = null;
    res.redirect("/login");
  }

  static update(req, res) {
    const { username, password, repeatPassword } = req.body;
    var salt = bcrypt.genSaltSync(10);
    var hash = bcrypt.hashSync(password, salt);
    const config = { username, password: hash };
    const string = JSON.stringify(config, null, 2);

    if (password !== repeatPassword) {
      return res.status(400).json({
        status: 400,
        name: "ValidationError",
        errors: {
          repeatPassword: ["Password didn't macth"],
        },
      });
    }

    fs.writeFile("./config/auth.json", string, (error) => {
      if (error) {
        return res
          .status(500)
          .json({ message: "Failed to update auth", error });
      }

      res.json({
        config: { username: config.username },
        message: "Config has been updated",
      });
    });
  }
}

module.exports = AuthController;
