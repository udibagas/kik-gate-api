"use strict";

const Gate = require("../models/gate");
const mqttClient = require("../services/mqtt.service");
const { DOMParser, XMLSerializer } = require("@xmldom/xmldom");

class LprController {
  static index(req, res) {
    let { ip } = req;
    ip = ip.slice(7);
    console.log(`LPR Endpoint hit by ${ip}`);
    console.log(req);

    try {
      const xmlString = req.file.buffer.toString();
      // console.log(xmlString);
      const xmlDoc = new DOMParser().parseFromString(xmlString, "text/xml");
      const licensePlate =
        xmlDoc.getElementsByTagName("licensePlate")[0].childNodes[0].nodeValue;

      Gate.findAll((err, gates) => {
        if (err) return res.send(err);

        let gate = { name: "Unknown" };
        let access = "Unknown";

        const gateIn = gates.find((g) => {
          return g.cameraIn["LPR Camera Address"] == ip;
        });

        const gateOut = gates.find((g) => {
          return g.cameraIn["LPR Camera Address"] == ip;
        });

        if (gateIn) {
          gate = gateIn;
          access = "IN";
        } else if (gateOut) {
          gate = gateOut;
          access = "OUT";
        }

        const publishedData = JSON.stringify({
          type: "lpr",
          licensePlate,
          gate: gate.name,
          access,
        });

        console.log(`Publish ${publishedData}`);
        mqttClient.publish("access", publishedData);

        res.json({ licensePlate });
      });
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: "Internal server error", error });
    }
  }

  static anpr(req, res) {
    const { ip, licensePlate } = req.query;

    Gate.findAll((err, gates) => {
      if (err) return res.send(err);

      let gate = { name: "unknown", host: "unknown" };
      let access = "unknown";

      const gateIn = gates.find((g) => {
        return g.cameraIn["LPR Camera Address"] == ip;
      });

      const gateOut = gates.find((g) => {
        return g.cameraIn["LPR Camera Address"] == ip;
      });

      if (gateIn) {
        gate = gateIn;
        access = "IN";
      }

      if (gateOut) {
        gate = gateOut;
        access = "OUT";
      }

      const publishedData = JSON.stringify({
        type: "lpr",
        licensePlate,
        host: gate.host,
        gate: gate.name,
        access,
      });

      console.log(`Publish ${publishedData}`);
      mqttClient.publish("access", publishedData);

      res.json({ licensePlate });
    });
  }
}

module.exports = LprController;
