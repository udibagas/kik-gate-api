"use strict";
const fs = require("fs");
const ejs = require("ejs");
const { exec } = require("child_process");

class ConfigController {
  static mqtt(req, res) {
    ejs.renderFile("./views/config/mqtt.ejs", {}, (err, content) => {
      if (err) return res.send(err);
      res.render("_layout", { content });
    });
  }

  static auth(req, res) {
    ejs.renderFile("./views/config/auth.ejs", {}, (err, content) => {
      if (err) return res.send(err);
      res.render("_layout", { content });
    });
  }

  static getMqttConfig(req, res) {
    const config = require("../config/mqtt.json");
    res.json(config);
  }

  static setMqttConfig(req, res) {
    const { host, port, username, password } = req.body;
    const config = { host, port, username, password };
    const string = JSON.stringify(config, null, 2);
    fs.writeFile("./config/mqtt.json", string, (error) => {
      if (error) {
        return res
          .status(500)
          .json({ message: "Failed to update config", error });
      }

      exec("pm2 restart mqtt", (err) => {
        if (err) console.error(err);
      });

      res.json({ config, message: "Config has been updated" });
    });
  }

  static restartMqttService(req, res) {
    exec("pm2 restart mqtt", (err) => {
      if (err)
        return res.json({
          message: "ERROR: Failed to restart MQTT service",
          err,
        });

      res.json({ message: "MQTT Service has been restarted successfully" });
    });
  }
}

module.exports = ConfigController;
