"use strict";

const ejs = require("ejs");

class Controller {
  static home(req, res) {
    const commands = require("../commands");
    const responses = require("../responses").filter((el) => !el.command);
    ejs.renderFile(
      "./views/home.ejs",
      { commands, responses },
      (err, content) => {
        if (err) return res.send(err);
        res.render("_layout", { content });
      }
    );
  }
}

module.exports = Controller;
