module.exports = [
  // response based on triggered input description
  {
    response: "W",
    description: "Kartu terdeteksi di gate IN",
    example: "WABC",
  },
  {
    response: "X",
    description: "Kartu terdeteksi di gate OUT",
    example: "XABC",
  },
  {
    response: "PT",
    description: "QR Code terdeteksi di gate IN",
    example:
      '{"idHeader": "xxxx", "noAju": "xxx", "idKontainer": "xxx", "nomorKontainer": "xxx"}',
  },
  {
    response: "QT",
    description: "QR Code terdeteksi di gate OUT",
    example:
      '{"idHeader": "xxxx", "noAju": "xxx", "idKontainer": "xxx", "nomorKontainer": "xxx"}',
  },
  {
    response: "IN1ON",
    description: "IN1 berubah status dari OFF menjadi ON",
    example: "",
  },
  {
    response: "IN1OFF",
    description: "IN1 berubah status dari ON menjadi OFF",
    example: "",
  },
  {
    response: "IN2ON",
    description: "IN2 berubah status dari OFF menjadi ON",
    example: "",
  },
  {
    response: "IN2OFF",
    description: "IN2 berubah status dari ON menjadi OFF",
    example: "",
  },
  {
    response: "IN3ON",
    description: "IN3 berubah status dari OFF menjadi ON",
    example: "",
  },
  {
    response: "IN3OFF",
    description: "IN3 berubah status dari ON menjadi OFF",
    example: "",
  },
  {
    response: "IN4ON",
    description: "IN4 berubah status dari OFF menjadi ON",
    example: "",
  },
  {
    response: "IN4OFF",
    description: "IN4 berubah status dari ON menjadi OFF",
    example: "",
  },

  {
    response: "PLAYEND",
    description: "Audio selesai diputar",
    example: "",
  },
  {
    response: "NOTRACK",
    description: "Audio tidak ditemukan",
    example: "",
  },

  // responses based on commands description
  { response: "OUT1ONOK", description: "", example: "", command: true },
  { response: "OUT2ONOK", description: "", example: "", command: true },
  { response: "OUT3ONOK", description: "", example: "", command: true },
  { response: "OUT4ONOK", description: "", example: "", command: true },
  { response: "OUT1OFFOK", description: "", example: "", command: true },
  { response: "OUT2OFFOK", description: "", example: "", command: true },
  { response: "OUT3OFFOK", description: "", example: "", command: true },
  { response: "OUT4OFFOK", description: "", example: "", command: true },
  { response: "OPEN1OK", description: "", example: "", command: true },
  { response: "CLOSE1OK", description: "", example: "", command: true },
  { response: "STOP1OK", description: "", example: "", command: true },
  { response: "OPEN2OK", description: "", example: "", command: true },
  { response: "CLOSE2OK", description: "", example: "", command: true },
  { response: "STOP2OK", description: "", example: "", command: true },
  { response: "PINGOK", description: "", example: "", command: true },
  { response: "STAT", description: "", example: "", command: true },
  { response: "TRIG1OK", description: "", example: "", command: true },
  { response: "TRIG2OK", description: "", example: "", command: true },
  { response: "TRIG3OK", description: "", example: "", command: true },
  { response: "TRIG4OK", description: "", example: "", command: true },
  { response: "MTOK", description: "", example: "", command: true },
  { response: "DSOK", description: "", example: "", command: true },
];
