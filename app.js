require("dotenv").config();
const express = require("express");
const cookieSession = require("cookie-session");
const app = express();
const port = 3000;

app.set("view engine", "ejs");
app.use(express.static("public"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(
  cookieSession({
    name: "session",
    keys: [process.env.SESSION_KEY],
    maxAge: 24 * 60 * 60 * 1000, // 24 hours
  })
);

app.use(require("./routes"));

app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});
