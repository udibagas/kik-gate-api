module.exports = {
  host: { type: "string", min: 1 },
  port: { type: "number" },
  username: { type: "string", optional: true },
  password: { type: "string", optional: true },
};
