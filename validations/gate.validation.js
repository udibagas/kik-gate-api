const { isIP } = require("net");

module.exports = {
  name: { type: "string", min: 1 },
  host: {
    type: "string",
  },
  port: { type: "number" },
  cameraIn: {
    type: "object",
    optional: true,
  },
  cameraOut: {
    type: "object",
    optional: true,
  },
};
