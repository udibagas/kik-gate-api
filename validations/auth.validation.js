module.exports = {
  username: { type: "string", min: 5 },
  password: { type: "string", min: 5 },
  repeatPassword: { type: "string", min: 5 },
};
